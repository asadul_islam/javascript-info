function slow(x) {
    console.log(`Called with ${x}`);
    return x;
}

function cachingDecorator(func) {
    let cache = new Map();

    return function(x) {
        if (cache.has(x)) {
            return cache.get(x);
        }

        let result = func(x);
        cache.set(x, result * result);
        return result;
    };
}

slow = cachingDecorator(slow);

console.log(slow(2));
console.log("Square : " + slow(2));

console.log(slow(3));
console.log("Square : " + slow(3));