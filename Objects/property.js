let user = {
    name: "John"
};

let descriptor = Object.getOwnPropertyDescriptor(user, "name"); //return properties of object

console.log(JSON.stringify(descriptor, null, 2));

//creating object properties
//if flag is not defined, it assumed as false
let user1 = {};

Object.defineProperty(user1, "name", {
    value: "John"
});

let descriptor1 = Object.getOwnPropertyDescriptor(user1, "name");

console.log(JSON.stringify(descriptor1, null, 2));

//making read-only property changing flag
Object.defineProperty(user, "name", {
    writable: false
});

user.name = "Pete"; //doesn't  change
console.log(user.name);

//Non - enumerable property
let user2 = {
    name: "John",
    toString() {
        return this.name;
    }
};

// By default, both our properties are listed:
for (let key in user2) console.log(key); // name, toString

Object.defineProperty(user2, "toString", {
    enumerable: false
});

// Now toString disappears:
for (let key in user2) console.log(key); // name

///non-configurable object property
let descriptor3 = Object.getOwnPropertyDescriptor(Math, "PI");
console.log(JSON.stringify(descriptor3, null, 2));
Math.PI = 3; //can't be changed
console.log(Math.PI);

//getter and setter methods
let user4 = {
    name: "John",
    surname: "Smith",

    get fullName() {
        return `${this.name} ${this.surname}`;
    },

    set fullName(value) {
        [this.name, this.surname] = value.split(" ");
    }
};

// set fullName is executed with the given value.
user4.fullName = "Alice Cooper";

console.log("\n" + user4.name); // Alice
console.log(user4.surname); // Cooper

//define object property
let user5 = {
    name: "John",
    surname: "Smith"
};

Object.defineProperty(user5, "fullName", {
    get() {
        return `${this.name} ${this.surname}`;
    },

    set(value) {
        [this.name, this.surname] = value.split(" ");
    }
});

console.log("\n" + user5.fullName); // John Smith

for (let key in user5) console.log(key);